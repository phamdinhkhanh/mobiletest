//
//  TableViewCellProductSearch.m
//  MobileTest
//
//  Created by PDInhKhanh on 2/17/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import "MTViewCellProductSearch.h"
#import <HCSStarRatingView/HCSStarRatingView.h>

@interface MTViewCellProductSearch()
@property (strong, nonatomic) IBOutlet HCSStarRatingView *startRatingView;

@end
@implementation MTViewCellProductSearch

- (void)awakeFromNib {
    // Initialization code
    self.startRatingView.userInteractionEnabled = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
