//
//  TableViewCellProductSearch.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/17/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTViewCellProductSearch : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageUser;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lblComment;
@property (strong, nonatomic) IBOutlet HCSStarRatingView *viewRating;
@end
