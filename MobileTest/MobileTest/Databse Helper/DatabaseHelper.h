//
//  databaseHelper.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTStorage.h"
#import "MTFetcherDB.h"

@interface DatabaseHelper : NSObject<MTStorage, MTFetcherDB>
@end
