//
//  databaseHelper.m
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import "DatabaseHelper.h"
#import "ProductModel.h"
#import "ProductManageObject.h"
#import "BrandModel.h"
#import "BrandManageObject.h"
#import "ReviewModel.h"
#import "ReviewManageObject.h"
#import "UserModel.h"
#import "UserManageObject.h"
#import "MTUtils.h"

@interface DatabaseHelper(){
    NSManagedObjectContext *localContextReView;
}
@end
@implementation DatabaseHelper
#pragma mark - Store databse ------
-(void)storeProducts:(NSArray*)productsArray{
    NSManagedObjectContext* localContext = [NSManagedObjectContext MR_contextForCurrentThread];
       [productsArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
           ProductModel* product = (ProductModel*)obj;
           ProductManageObject* productObject = [ProductManageObject MR_createInContext:localContext];
           
           productObject.objectId           = product.objectId;
           productObject.availabilityStatus = product.availabilityStatus;
           productObject.colour             = product.colour;
           productObject.descProduct        = product.descProduct;
           productObject.price              = product.price;
           productObject.productName        = product.productName;
           productObject.dateCreated = [MTUtils dateFormatter:product.dateCreated.isoDate];
           
           BrandManageObject* brandObject = [BrandManageObject MR_findFirstByAttribute:@"objectId" withValue:product.brandId.objectId inContext:localContext];
           if(!brandObject){
               brandObject = [BrandManageObject MR_createInContext:localContext];
                brandObject.objectId = product.brandId.objectId;
           }
           
           [productObject setBrand:brandObject];
           [brandObject addProductObject:productObject];
       }];
    [localContext MR_saveToPersistentStoreAndWait];
}

-(void)storeBrands:(NSArray*)brandsArray{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        [brandsArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            BrandModel* brand = (BrandModel*)obj;
            BrandManageObject* brandObject = [BrandManageObject MR_createInContext:localContext];
            brandObject.objectId = brand.objectId;
            brandObject.descBrand   = brand.descBrand;
            brandObject.name        = brand.nameBrand;
        }];
    } completion:^(BOOL success, NSError *error) {
        if(error)
            NSLog(@"Save with error %@", error.description);
        else{
            NSLog(@"Save without error");
        }
    }];
}

-(void)storeReviews:(NSArray*)reviewsArray{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        [reviewsArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ReviewModel* review = (ReviewModel*)obj;
            ReviewManageObject* reviewObject = [ReviewManageObject MR_createInContext:localContext];
            
            reviewObject.objectId = review.objectId;
            reviewObject.comment  = review.comment;
            if([review.rating intValue] > 10){
                reviewObject.rating   = [NSNumber numberWithInt:10];
            }else{
                reviewObject.rating   = review.rating;
            }
            reviewObject.updateAt = [MTUtils dateFormatter:review.updatedAt];
            
            UserManageObject* userObject = [UserManageObject MR_findFirstByAttribute:@"objectId" withValue:review.userId.objectId inContext:localContext];
            if(!userObject){
                userObject = [UserManageObject MR_createInContext:localContext];
                userObject.objectId = review.userId.objectId;
            }
            [reviewObject setUser:userObject];
            
            ProductManageObject* productObject = [ProductManageObject MR_findFirstByAttribute:@"objectId" withValue:review.productId.objectId inContext:localContext];
            if(!productObject){
                productObject = [ProductManageObject MR_createInContext:localContextReView];
                productObject.objectId = review.productId.objectId;
            }

            [reviewObject setProduct:productObject];
        
        }];
    } completion:^(BOOL success, NSError *error) {
        if(error)
            NSLog(@"Save with error %@", error.description);
        else{
            NSLog(@"Save without error");
        }
    }];

}

-(void)storeUsers:(NSArray*)usersArray{
     NSManagedObjectContext* localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        [usersArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            UserModel* user = (UserModel*)obj;
            UserManageObject* userObject = [UserManageObject MR_findFirstByAttribute:@"objectId" withValue:user.objectId inContext:localContext];
            if(!userObject){
                userObject = [UserManageObject MR_createInContext:localContext];
                userObject.objectId = user.objectId;
            }
            userObject.email = user.email;
            userObject.username  = user.userName;
            userObject.userType  = @"Merchant";
            if(user.isCustomer == YES){
                userObject.userType = @"Customer";
            }
            userObject.dateOfBirth = [MTUtils dateFormatter:user.dateOfBirth.isoDate];
        }];
    [localContext MR_saveToPersistentStoreAndWait];
}

#pragma mark - Fetch database -------

-(void)getBrandWithObjectId:(NSString *)objectId onComplete:(MTObjectBlock)block{
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    BrandManageObject* brandObject = [BrandManageObject MR_findFirstByAttribute:@"objectId" withValue:objectId inContext:localContext];
    block(brandObject, nil);
}

-(void)getUserWithObjectId:(NSString *)objectId onComplete:(MTObjectBlock)block{
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    UserManageObject* userObject = [UserManageObject MR_findFirstByAttribute:@"objectId" withValue:objectId inContext:localContext];
    block(userObject, nil);
}

-(void)getBrandWithbrandName:(NSString *)brandName onComplete:(MTObjectBlock)block{
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    BrandManageObject* brandObject = [BrandManageObject MR_findFirstByAttribute:@"name" withValue:brandName inContext:localContext];
    block(brandObject, nil);
}

-(int)getReviewCountWithProductId:(NSString*)productId;
{
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF.product.objectId == %@", productId];
    return (int)[ReviewManageObject MR_countOfEntitiesWithPredicate:filter];
}

-(NSArray *)getEmail{
    return [UserManageObject MR_findAll];
}

-(id)getProductWithProductId:(NSString *)productId{
    id productObject = [ProductManageObject MR_findFirstByAttribute:@"objectId" withValue:productId];
    return productObject;
}

- (id)checkEmailUserIsExist:(NSString *)email{
    id userObject = [UserManageObject MR_findFirstByAttribute:@"email" withValue:email];
    return userObject;
}
@end
