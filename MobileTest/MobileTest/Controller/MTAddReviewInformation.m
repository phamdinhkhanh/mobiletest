//
//  MTAddReviewInformation.m
//  MobileTest
//
//  Created by PDInhKhanh on 2/26/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import "MTAddReviewInformation.h"
#import "MTAppDataManager.h"
#import "UIView+Toast.h"
#import "ProductManageObject.h"
#import <AVFoundation/AVFoundation.h>

#define App_Id_Speech_Kit @"NMDPPRODUCTION_TMA_Solution_MobileTest_20160219105315"
#define ServerHost_Speech_Kit @"hlp.nmdp.nuancemobility.net"
#define ServerPort_Speech_Kit @"443"
#define App_Key_Speech_Kit @"f27b1039a03ad6fdebdbcc129996299df6e297ea25c924ae691ae26eb9ecfeac670ee50848bb053497d146306770ce5d526547772e6c259786070540f403ec5b"
#define App_ServerUrl @"nmsps://%@@%@:%@"

#define kOFFSET_FOR_KEYBOARD 30.0

// State Logic: IDLE -> LISTENING -> PROCESSING -> repeat
enum {
    SKSIdle = 1,
    SKSListening = 2,
    SKSProcessing = 3
};
typedef NSUInteger SKSState;

@interface MTAddReviewInformation ()<SKTransactionDelegate,AVCaptureMetadataOutputObjectsDelegate, UITextFieldDelegate>{
    
    // variable Framework Speech to Text
    SKSession* m_skSession;
    SKTransaction* m_skTransaction;
    SKSState m_state;
    NSString* m_language;
    NSString* m_recognitionType;
    SKTransactionEndOfSpeechDetection m_endpointer;
    
    // variable Framework QR code
    AVCaptureSession* m_captureSession;
    AVCaptureVideoPreviewLayer* m_videoPreviewLayer;
    AVAudioPlayer* m_audioPlayer;
    BOOL m_isReading;
    
    UIAlertView*  m_alertView;
    UITapGestureRecognizer* m_tapGesture;
}

@property (strong, nonatomic) IBOutlet UITextField *txtProductID;
@property (strong, nonatomic) IBOutlet UIButton *btnSpeechToText;
@property (strong, nonatomic) IBOutlet UIButton *btnQRCode;
@property (strong, nonatomic) IBOutlet UIView *viewPreview;

- (IBAction)addReview:(id)sender;
@end

@implementation MTAddReviewInformation

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter ] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter ] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    m_tapGesture = [[UITapGestureRecognizer alloc]
                    initWithTarget:self
                    action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:m_tapGesture];
    self.viewPreview.hidden = YES;
    [self initSpeechToText];
    [self initQRCode];
    [self startFlecthingProduct];


}

/**
 *  Fetch product from sever API
 */
- (void)startFlecthingProduct{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [[MTAppDataManager shareInstance] getAllProductsWithOnCompleteBlock:^(NSArray *results, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        if(!error){
        }else{
            [self showAlertWithTitle:nil message:error.userInfo[@"NSLocalizedDescription"] cancel:@"OK" sender:self];
        }
    }];
}

-(void)hideKeyboard{
    [self.txtProductID resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - SpeechToText code ---

/**
 *  Init SpeechTotext
 */
-(void)initSpeechToText{
    m_recognitionType = SKTransactionSpeechTypeDictation;
    m_endpointer = SKTransactionEndOfSpeechDetectionShort;
    m_language = @"eng-USA";
    
    m_state = SKSIdle;
    m_skTransaction = nil;
    
    // Create a session
    m_skSession = [[SKSession alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:App_ServerUrl, App_Id_Speech_Kit, ServerHost_Speech_Kit, ServerPort_Speech_Kit]] appToken:App_Key_Speech_Kit];
    
    if (!m_skSession) {
        [self showAlertWithTitle:@"SpeechKit" message:@"Failed to initialize SpeechKit session." cancel:@"OK" sender:self];
    }
    
    [self loadEarcons];
    
}

- (void)loadEarcons{
    NSString* startEarconPath = [[NSBundle mainBundle] pathForResource:@"sk_start" ofType:@"pcm"];
    NSString* stopEarconPath = [[NSBundle mainBundle] pathForResource:@"sk_stop" ofType:@"pcm"];
    NSString* errorEarconPath = [[NSBundle mainBundle] pathForResource:@"sk_error" ofType:@"pcm"];
    
    SKPCMFormat* audioFormat = [[SKPCMFormat alloc] init];
    audioFormat.sampleFormat = SKPCMSampleFormatSignedLinear16;
    audioFormat.sampleRate = 16000;
    audioFormat.channels = 1;
    
    // Attach them to the session
    
    m_skSession.startEarcon = [[SKAudioFile alloc] initWithURL:[NSURL fileURLWithPath:startEarconPath] pcmFormat:audioFormat];
    m_skSession.endEarcon = [[SKAudioFile alloc] initWithURL:[NSURL fileURLWithPath:stopEarconPath] pcmFormat:audioFormat];
    m_skSession.errorEarcon = [[SKAudioFile alloc] initWithURL:[NSURL fileURLWithPath:errorEarconPath] pcmFormat:audioFormat];
    
}

- (void)resetTransaction
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        m_skTransaction = nil;
        [self.btnSpeechToText setTitle:@"Speech To Text" forState:UIControlStateNormal];
        [self.btnSpeechToText setEnabled:YES];
    }];
}

/**
 *  action speech to text
 *
 *  @param sender: sender action
 */
- (IBAction)speechToText:(id)sender {
    switch (m_state) {
        case SKSIdle:
            [self recognize];
            break;
        case SKSListening:
            [self stopRecording];
            break;
        case SKSProcessing:
            [self cancel];
            break;
        default:
            break;
    }
}

# pragma mark - SKTransactionDelegate ---

- (void)transactionDidBeginRecording:(SKTransaction *)transaction
{
    m_state = SKSListening;
    [self.btnSpeechToText setTitle:@"Listening.." forState:UIControlStateNormal];
}

- (void)transactionDidFinishRecording:(SKTransaction *)transaction
{
    m_state = SKSProcessing;
    [self.btnSpeechToText setTitle:@"Processing.." forState:UIControlStateNormal];
}

- (void)transaction:(SKTransaction *)transaction didReceiveRecognition:(SKRecognition *)recognition
{
    self.txtProductID.text = recognition.text;
    m_state = SKSIdle;
//    if(self.txtProductID.text.length > 0){
//        ProductManageObject* productObject = (ProductManageObject*)[[MTAppDataManager shareInstance] getProductWithProductId:self.txtProductID.text];
//        if(productObject == nil){
//            //self.txtProductName.text = @"";
//        }else{
//            //self.txtProductName.text = productObject.productName;
//        }
//    }else{
//        //self.txtProductName.text = @"";
//    }
}

- (void)transaction:(SKTransaction *)transaction didReceiveServiceResponse:(NSDictionary *)response
{
}

- (void)transaction:(SKTransaction *)transaction didFinishWithSuggestion:(NSString *)suggestion
{
    m_state = SKSIdle;
    [self resetTransaction];
}

- (void)transaction:(SKTransaction *)transaction didFailWithError:(NSError *)error suggestion:(NSString *)suggestion
{
    m_state = SKSIdle;
    [self resetTransaction];
}

- (void)recognize
{
    // Start listening to the user.
    [self.view makeToast:@"Start recording......"
                duration:3.0
                position:CSToastPositionTop
                   style:nil];
    [self.btnSpeechToText setTitle:@"Stop" forState:UIControlStateNormal];
    
    m_skTransaction = [m_skSession recognizeWithType:m_recognitionType
                                           detection:m_endpointer
                                            language:m_language
                                            delegate:self];
}

- (void)stopRecording
{
    // Start listening to the user.
    [self.view makeToast:@"Stop recording......"
                duration:3.0
                position:CSToastPositionTop
                   style:nil];
    
    [self.btnSpeechToText setTitle:@"Speech To Text" forState:UIControlStateNormal];
    
    // Stop recording the user.
    [m_skTransaction stopRecording];
    // Disable the button until we received notification that the transaction is completed.
    [self.btnSpeechToText setEnabled:NO];
}

- (void)cancel
{
    // Start listening to the user.
    [self.view makeToast:@"Cancel recognize......"
                duration:3.0
                position:CSToastPositionTop
                   style:nil];
    
    [self.btnSpeechToText setTitle:@"Speech To Text" forState:UIControlStateNormal];
    
    // Cancel the Reco transaction.
    // This will only cancel if we have not received a response from the server yet.
    [m_skTransaction cancel];
}

# pragma mark - QR code ---
/**
 *  init QRCode
 */
-(void)initQRCode{
    // Do any additional setup after loading the view, typically from a nib.
    
    // Initially make the captureSession object nil.
    m_captureSession = nil;
    
    // Set the initial value of the flag to NO.
    m_isReading = NO;
    
    // Begin loading the sound effect so to have it ready for playback when it's needed.
    [self loadBeepSound];
}

-(void)loadBeepSound{
    // Get the path to the beep.mp3 file and convert it to a NSURL object.
    NSString *beepFilePath = [[NSBundle mainBundle] pathForResource:@"beep" ofType:@"mp3"];
    NSURL *beepURL = [NSURL URLWithString:beepFilePath];
    
    NSError *error;
    
    // Initialize the audio player object using the NSURL object previously set.
    m_audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:beepURL error:&error];
    if (error) {
        // If the audio player cannot be initialized then log a message.
        NSLog(@"Could not play beep file.");
        NSLog(@"%@", [error localizedDescription]);
    }
    else{
        // If the audio player was successfully initialized then load it in memory.
        [m_audioPlayer prepareToPlay];
    }
}


- (IBAction)takeQRCode:(id)sender {
    [self hideKeyboard];
    if (!m_isReading) {
        // This is the case where the app should read a QR code when the start button is tapped.
        if ([self startReading]) {
            // If the startReading methods returns YES and the capture session is successfully
            // running, then change the start button title and the status message.
            [self.btnQRCode setTitle:@"Stop" forState:UIControlStateNormal];
        }
    }
    else{
        // In this case the app is currently reading a QR code and it should stop doing so.
        [self stopReading];
        // The bar button item's title should change again.
        [self.btnQRCode setTitle:@"QR Code" forState:UIControlStateNormal];
        self.viewPreview.hidden = YES;
    }
    
    // Set to the flag the exact opposite value of the one that currently has.
    m_isReading = !m_isReading;
}

- (BOOL)startReading {
    NSError *error;
    // show view to preview
    self.viewPreview.hidden = NO;
    // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
    // as the media type parameter.
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Get an instance of the AVCaptureDeviceInput class using the previous device object.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        // If any error occurs, simply log the description of it and don't continue any more.
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    // Initialize the captureSession object.
    m_captureSession = [[AVCaptureSession alloc] init];
    // Set the input device on the capture session.
    [m_captureSession addInput:input];
    
    
    // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [m_captureSession addOutput:captureMetadataOutput];
    
    // Create a new serial dispatch queue.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    m_videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:m_captureSession];
    [m_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [m_videoPreviewLayer setFrame:_viewPreview.layer.bounds];
    [_viewPreview.layer addSublayer:m_videoPreviewLayer];
    
    
    // Start video capture.
    [m_captureSession startRunning];
    
    return YES;
}


-(void)stopReading{
    // Stop video capture and make the capture session object nil.
    [m_captureSession stopRunning];
    m_captureSession = nil;
    
    // Remove the video preview layer from the viewPreview view's layer.
    [m_videoPreviewLayer removeFromSuperlayer];
}

#pragma mark - AVCaptureMetadataOutputObjectsDelegate method implementation

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        // Get the metadata object.
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
            [self performSelectorOnMainThread:@selector(updateButton:) withObject:[metadataObj stringValue]  waitUntilDone:NO];
            
            m_isReading = NO;
            
            // If the audio player is not nil, then play the sound effect.
            if (m_audioPlayer) {
                [m_audioPlayer play];
            }
        }
    }
}

-(void)updateButton:(NSString*)productId{
    self.txtProductID.text = productId;
    [self.btnQRCode setTitle:@"QR code" forState:UIControlStateNormal];
    [self stopReading];
    self.viewPreview.hidden = YES;
//    if(self.txtProductID.text.length > 0){
//        ProductManageObject* productObject = (ProductManageObject*)[[MTAppDataManager shareInstance] getProductWithProductId:self.txtProductID.text];
//        if(productObject == nil){
//            //self.txtProductName.text = @"";
//        }else{
//            //self.txtProductName.text = productObject.productName;
//        }
//    }else{
//            //self.txtProductName.text = @"";
//    }
}

#pragma mark - Textfield delegate ------
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}


#pragma mark -- Keyboard --
/* ------------------------------------------------ */
- (void)keyboardWillShow:(NSNotification*)aNotification {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    CGRect rect = self.view.frame;
    rect.origin.y -= kOFFSET_FOR_KEYBOARD;
    rect.size.height += kOFFSET_FOR_KEYBOARD;
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification*)aNotification{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    CGRect rect = self.view.frame;
    rect.origin.y += kOFFSET_FOR_KEYBOARD;
    rect.size.height -= kOFFSET_FOR_KEYBOARD;
    self.view.frame = rect;
    [UIView commitAnimations];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)addReview:(id)sender{
    if(self.txtProductID.text.length > 0){
        ProductManageObject* productObject = (ProductManageObject*)[[MTAppDataManager shareInstance] getProductWithProductId:self.txtProductID.text];
        if(productObject == nil){
            //self.txtProductName.text = @"";
        }else{
            //self.txtProductName.text = productObject.productName;
        }
    }else{
        //self.txtProductName.text = @"";
    }
}

- (void)dealloc{
    if (![[self.navigationController viewControllers] containsObject: self])
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

-(void)willMoveToParentViewController:(UIViewController *)parent{
    if(parent != nil){
        [self closeAlertView];
    }
}

- (void) showAlertWithTitle:(NSString *)title message:(NSString*) message cancel:(NSString *)cancel sender:(id) sender{
    if([MTUtils checkIosVersion] <= 7){
        m_alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:sender cancelButtonTitle:cancel otherButtonTitles:nil];
        [m_alertView show];
    }else{
        UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:cancel style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
        }]];
        [sender presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)closeAlertView{
    if([MTUtils checkIosVersion] <= 7){
        if(m_alertView != nil){
            [m_alertView dismissWithClickedButtonIndex:-1 animated:NO];
        }
    }else{
        if([[self presentedViewController] respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]){
            [[self presentedViewController] dismissViewControllerAnimated:YES completion:nil];
        }
    }
}
@end
