//
//  VIewDetailController.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/17/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductManageObject.h"

@interface MTViewDetailController : UIViewController
@property (nullable, nonatomic, strong) ProductManageObject* prodcutObject;
@property (nullable, nonatomic, strong) NSString* averageRating;
@property (nonatomic, assign)BOOL isNeedReload;
@end
