//
//  VIewDetailController.m
//  MobileTest
//
//  Created by PDInhKhanh on 2/17/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import "MTVIewDetailController.h"
#import "MTAppDataManager.h"
#import "UserModel.h"
#import "ReviewManageObject.h"
#import "BrandManageObject.h"
#import "MTViewCellProductSearch.h"
#import "MTAddReviewProduct.h"
#import "MTTableViewControllerProduct.h"
#import "UserManageObject.h"


@interface MTViewDetailController ()<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>{
    UIAlertView* m_alertView;
}
@property (strong, nonatomic) IBOutlet UILabel *lblProductName;
@property (strong, nonatomic) IBOutlet UILabel *lblBrand;
@property (strong, nonatomic) IBOutlet UILabel *lblDescProduct;
@property (strong, nonatomic) IBOutlet UITableView *tvReview;
@property (strong, nonatomic) IBOutlet UILabel *lblAverageRating;
- (IBAction)addReview:(id)sender;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@end

@implementation MTViewDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"product.objectId == %@", self.prodcutObject.objectId];
    
    self.fetchedResultsController =  [ReviewManageObject MR_fetchAllSortedBy:@"updateAt" ascending:NO withPredicate:filter groupBy:nil delegate:self];
    [self setUpUI];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(self.isNeedReload == YES){
        [self startFletchingReviewWithProductId:self.prodcutObject.objectId];
        self.isNeedReload = NO;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        NSArray* viewControllers = self.navigationController.viewControllers;
        UIViewController *aViewController = [viewControllers objectAtIndex:viewControllers.count - 1];
        if([aViewController isKindOfClass:[MTTableViewControllerProduct class]])
        {
            MTTableViewControllerProduct* viewDetailController = (MTTableViewControllerProduct*)aViewController;
            viewDetailController.isNeedReload = self.isNeedReload;
        }
    }
    [super viewWillDisappear:animated];
}

-(void)startFletchingReviewWithProductId:(NSString*)productId{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [[MTAppDataManager shareInstance] getAllReviewsWithProductId:productId OnCompleteBlock:^(NSArray *results, NSError *error) {
        if(!error){
            [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
             self.lblAverageRating.text = [NSString stringWithFormat:@"Average rating:%d/%lu", [MTUtils calculateAverageRating:results], results.count * 10];
        }else{
            [self showAlertWithTitle:nil message:error.userInfo[@"NSLocalizedDescription"] cancel:@"OK" sender:self];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *  set up UI to view on view controller
 */
-(void)setUpUI{
    self.lblProductName.text = self.prodcutObject.productName;
    self.lblBrand.text = self.prodcutObject.brand.name;
    self.lblDescProduct.text = self.prodcutObject.descProduct;
    if(self.isNeedReload == NO){
        self.lblAverageRating.text = self.averageRating;
    }
}

#pragma mark - Tableview Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id  sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MTViewCellProductSearch* cell = (MTViewCellProductSearch*)[tableView dequeueReusableCellWithIdentifier:@"CellProductSearch"];
    if (cell == nil)
    {
        cell = [[MTViewCellProductSearch alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:@"CellProduct"];
    }
    ReviewManageObject* reviewObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.lblUserName.text = reviewObject.user.username;
    cell.lblComment.text  = reviewObject.comment;
    cell.imageUser.image = [UIImage imageNamed:@"User"];
    cell.viewRating.value = [reviewObject.rating floatValue];
    
    return cell;

}

#pragma mark ======================================
#pragma mark NSFetchedResultsControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller{
    [self.tvReview beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    [self.tvReview endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type{
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tvReview insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tvReview deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath{
    
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tvReview insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tvReview deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            //[self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [self.tvReview deleteRowsAtIndexPaths:[NSArray
                                                    arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self.tvReview insertRowsAtIndexPaths:[NSArray
                                                    arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"PushToAddReviewController"]){
        MTAddReviewProduct* destViewController =  (MTAddReviewProduct*)[segue destinationViewController];
        destViewController.isDetailView = YES;
        destViewController.productName  = self.prodcutObject.productName;
        destViewController.productId    = self.prodcutObject.objectId;
    }
}

- (void) showAlertWithTitle:(NSString *)title message:(NSString*) message cancel:(NSString *)cancel sender:(id) sender{
    if([MTUtils checkIosVersion] <= 7){
        m_alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:sender cancelButtonTitle:cancel otherButtonTitles:nil];
        [m_alertView show];
    }else{
        UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:cancel style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
        }]];
        [sender presentViewController:alertController animated:YES completion:nil];
    }
}

-(void)willMoveToParentViewController:(UIViewController *)parent{
    if(parent != nil){
//        if ([parent isKindOfClass:[MTTableViewControllerProduct class]]) {
//            MTTableViewControllerProduct* tableViewControllerProduct = (MTTableViewControllerProduct*)parent;
//            tableViewControllerProduct.isNeedReload = YES;
//        }
        [self closeAlertView];
    }
}

- (void)closeAlertView{
    if([MTUtils checkIosVersion] <= 7){
        if(m_alertView != nil){
            [m_alertView dismissWithClickedButtonIndex:-1 animated:NO];
        }
    }else{
        if([[self presentedViewController] respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]){
            [[self presentedViewController] dismissViewControllerAnimated:YES completion:nil];
        }
    }
}
- (IBAction)addReview:(id)sender {
}
@end
