//
//  MTTableViewControllerProduct.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/17/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductManageObject.h"

@interface MTTableViewControllerProduct : UITableViewController
@property (nullable, nonatomic, strong) NSString* brandName;
@property (nullable, nonatomic, strong) NSString* brandId;
@property (nonatomic, assign) BOOL isNeedReload;
@end
