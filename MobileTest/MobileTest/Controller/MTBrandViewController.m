//
//  MTBrandViewController.m
//  MobileTest
//
//  Created by PDInhKhanh on 2/18/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import "MTBrandViewController.h"
#import "BrandManageObject.h"
#import "BrandModel.h"
#import "MTAppDataManager.h"
#import "MTTableViewControllerProduct.h"
#import "MTAddReviewProduct.h"

@interface MTBrandViewController ()<UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>{
    UIAlertView* m_alertView;
    NSMutableArray* m_arrayBrand;
    NSArray* m_arrayContent;
}
@property (strong, nonatomic) IBOutlet UISearchBar *searchBrand;
@property (strong, nonatomic) IBOutlet UIView *viewAutoComplete;
@property (strong, nonatomic) IBOutlet UITableView *tvAutoComplete;

@end

@implementation MTBrandViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    m_arrayBrand = [[NSMutableArray alloc] init];
    m_arrayContent = [[NSArray alloc] init];
    [self starFletchingUser];
    [self startFletchingBrand];
    [self setUpViewAutoComplete];
    [self setUpNavigationController];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryFolderPath = [paths firstObject];
    NSLog(@"path DB %@", libraryFolderPath);

}


/**
 *  set Up navigation controller
 */
-(void)setUpNavigationController{
    UIBarButtonItem *addReviewBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Add Review" style:UIBarButtonItemStylePlain target:self action:@selector(addReview:)];
    self.navigationItem.rightBarButtonItem = addReviewBarButton;
}

/**
 *  push to view add review
 *
 *  @param sender sender description
 */
-(void)addReview:(id)sender{
    NSArray* allHudView = [MBProgressHUD allHUDsForView:self.view];
    if(allHudView.count == 0){
        MTAddReviewProduct* destViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MTAddReviewInfoController"];
        [self.navigationController pushViewController:destViewController animated:YES];
    }
}

/**
 *  set up UI view auto complete
 */
-(void)setUpViewAutoComplete{
    self.viewAutoComplete.hidden = YES;
    self.viewAutoComplete.layer.borderWidth = 2;
    self.viewAutoComplete.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.viewAutoComplete.layer.cornerRadius = 10.0f;
    self.tvAutoComplete.layer.cornerRadius = 10.0f;
}

/**
 *  Fetch brand from API
 */
- (void)startFletchingBrand{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[MTAppDataManager shareInstance] getAllBrandsWithOnCompleteBlock:^(NSArray *results, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if(!error){
            [m_arrayBrand addObjectsFromArray:results];
        }else{
            [self showAlertWithTitle:nil message:error.userInfo[@"NSLocalizedDescription"] cancel:@"OK" sender:self];
        }
    }];
}

/**
 *  Fetch user from API
 */
-(void)starFletchingUser{
    [[MTAppDataManager shareInstance] getAllUsersWithOnCompleteBlock:^(NSArray *results, NSError *error) {
        if(!error){
        }else{
            [self showAlertWithTitle:nil message:error.userInfo[@"NSLocalizedDescription"] cancel:@"OK" sender:self];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - TableView Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return m_arrayContent.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"CellAutoComplete"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:@"CellAutoComplete"];
    }
    BrandModel* brand = (BrandModel*)[m_arrayContent objectAtIndex:indexPath.row];
    cell.textLabel.text = brand.nameBrand;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.searchBrand.text = [self.tvAutoComplete cellForRowAtIndexPath:indexPath].textLabel.text;
    self.viewAutoComplete.hidden = YES;
    [[MTAppDataManager shareInstance] getBrandWithbrandName:self.searchBrand.text onComplete:^(id obj, NSError *error) {
        if(obj){
            BrandManageObject* brandObject = (BrandManageObject*)obj;
            MTTableViewControllerProduct* destViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MTTableViewControllerProduct"];
            destViewController.brandName = brandObject.name;
            destViewController.brandId  = brandObject.objectId;
            destViewController.isNeedReload = NO;
            [self.navigationController pushViewController:destViewController animated:YES];
        }
    }];
}

#pragma mak - Search Delagate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSPredicate *sPredicate = [NSPredicate predicateWithFormat:@"SELF.nameBrand contains[c] %@", self.searchBrand.text];
    m_arrayContent = [m_arrayBrand filteredArrayUsingPredicate:sPredicate];
    if(m_arrayContent.count > 0){
        [self.tvAutoComplete reloadData];
        self.viewAutoComplete.hidden = NO;
    }else{
        [self showAlertWithTitle:nil message:@"Don't have branch name match with keyword" cancel:@"OK" sender:self];
        self.viewAutoComplete.hidden = YES;
    }
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    self.searchBrand.text = @"";
    self.viewAutoComplete.hidden = YES;
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(self.searchBrand.text.length == 0){
        self.viewAutoComplete.hidden = YES;
    }else{
        NSPredicate *sPredicate = [NSPredicate predicateWithFormat:@"SELF.nameBrand contains[c] %@", self.searchBrand.text];
        m_arrayContent = [m_arrayBrand filteredArrayUsingPredicate:sPredicate];
        if(m_arrayContent.count > 0){
            [self.tvAutoComplete reloadData];
            self.viewAutoComplete.hidden = NO;
        }else{
            self.viewAutoComplete.hidden = YES;
        }
    }
}


- (void) showAlertWithTitle:(NSString *)title message:(NSString*) message cancel:(NSString *)cancel sender:(id) sender{
    if([MTUtils checkIosVersion] <= 7){
        m_alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:sender cancelButtonTitle:cancel otherButtonTitles:nil];
        [m_alertView show];
    }else{
        UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:cancel style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
        }]];
        [sender presentViewController:alertController animated:YES completion:nil];
    }
}
@end
