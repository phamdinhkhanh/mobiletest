//
//  MTTableViewControllerProduct.m
//  MobileTest
//
//  Created by PDInhKhanh on 2/17/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import "MTTableViewControllerProduct.h"
#import "MTAppDataManager.h"
#import "MTViewCellProduct.h"
#import "ProductManageObject.h"
#import "ProductModel.h"
#import "ReviewManageObject.h"
#import "BrandManageObject.h"
#import "MTViewDetailController.h"
#import "MTAddReviewProduct.h"

@interface MTTableViewControllerProduct ()<UINavigationBarDelegate, NSFetchedResultsControllerDelegate>
{
    UIAlertView* m_alertView;
    NSMutableArray* m_arrayReivew;
}
@property (nonatomic, strong) NSFetchedResultsController* fetchedResultsController;
@end

@implementation MTTableViewControllerProduct
@synthesize fetchedResultsController = _fetchedResultsController;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNavigationController];
    m_arrayReivew = [[NSMutableArray alloc] init];
    [self startFlecthingProductWithBrandName:self.brandName];
    [self initFetchedResultsController];
    self.title = self.brandName;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(self.isNeedReload == YES){
        [self startFlecthingProductWithBrandName:self.brandName];
        self.isNeedReload = NO;
    }
}

/**
 *  set Up navigation controller
 */
-(void)setUpNavigationController{
    UIBarButtonItem *addReviewBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Add Review" style:UIBarButtonItemStylePlain target:self action:@selector(addReview:)];
     UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Search Brand" style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
    self.navigationItem.rightBarButtonItem = addReviewBarButton;
    self.navigationItem.backBarButtonItem = backBarButton;
}

/**
 *  push to view add review
 *
 *  @param sender sender description
 */
-(void)addReview:(id)sender{
    NSArray* allHudView = [MBProgressHUD allHUDsForView:self.view];
    if(allHudView.count == 0){
        MTAddReviewProduct* destViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MTAddReviewInfoController"];
        [self.navigationController pushViewController:destViewController animated:YES];
    }
}

/**
 * Description
 */
-(void)back:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/**
 *  init fetch result controller
 */
- (void)initFetchedResultsController{
    [NSFetchedResultsController deleteCacheWithName:@"Product"];
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        NSLog(@"error %@, %@", error, [error userInfo]);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)startFlecthingProductWithBrandName:(NSString*)brandName{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [[MTAppDataManager shareInstance] getAllProductsWithBrandName:brandName OnCompleteBlock:^(NSArray *results, NSError *error) {
        if(!error){
            if(results.count > 0){
                [self startFletchingReviewWithArrayProductId:results];
            }else{
                [self showAlertWithTitle:nil message:@"Don't have product of this brand" cancel:@"Ok" sender:self];
            }
        }else{
            [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
            [self showAlertWithTitle:nil message:error.userInfo[@"NSLocalizedDescription"] cancel:@"OK" sender:self];
        }
    }];
}


-(void)startFletchingReviewWithArrayProductId:(NSArray*)productId{
    [[MTAppDataManager shareInstance] getAllReviewsWithProductIdArray:productId OnCompleteBlock:^(NSArray *results, NSError *error) {
        if(!error){
            [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
            m_arrayReivew = [NSMutableArray arrayWithArray:results];
            [self.tableView reloadData];
        }else{
            [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
            [self showAlertWithTitle:nil message:error.userInfo[@"NSLocalizedDescription"] cancel:@"OK" sender:self];
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id  sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MTViewCellProduct* cell = (MTViewCellProduct*)[tableView dequeueReusableCellWithIdentifier:@"CellProduct"];
    if (cell == nil)
    {
        cell = [[MTViewCellProduct alloc] initWithStyle:UITableViewCellStyleDefault
                                 reuseIdentifier:@"CellProduct"];
    }
    ProductManageObject* productObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.lblName.text = productObject.productName;
    cell.lblStatus.text = productObject.availabilityStatus;
    cell.lblDescProduct.text = productObject.descProduct;
    cell.lblPrice.text = [NSString stringWithFormat:@"$ %@", [productObject.price stringValue]];
    cell.lblDateTime.text = [NSDateFormatter localizedStringFromDate:productObject.dateCreated
                                                           dateStyle:NSDateFormatterShortStyle
                                                           timeStyle:NSDateFormatterFullStyle];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF.productId.objectId == %@", productObject.objectId];
    NSArray* reviewArray =  [m_arrayReivew filteredArrayUsingPredicate:filter];
    
    cell.lblReview.text = [NSString stringWithFormat:@"Average rating:%d/%lu", [MTUtils calculateAverageRating:reviewArray], reviewArray.count * 10];
    cell.clipsToBounds = YES;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ProductManageObject* productObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    MTViewDetailController* destViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MTViewDetailController"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF.productId.objectId == %@", productObject.objectId];
    
    NSArray* reviewArray =  [m_arrayReivew filteredArrayUsingPredicate:filter];
    destViewController.prodcutObject = productObject;
    destViewController.averageRating = [NSString stringWithFormat:@"Average rating:%d/%lu", [MTUtils calculateAverageRating:reviewArray], reviewArray.count * 10];
    destViewController.isNeedReload = NO;
    [self.navigationController pushViewController:destViewController animated:YES];
}


#pragma mark ======================================
#pragma mark NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"ProductManageObject" inManagedObjectContext:[NSManagedObjectContext MR_rootSavingContext]];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                              initWithKey:@"dateCreated" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"brand.name = %@",self.brandName];
    
    [fetchRequest setPredicate:predicate];
    [fetchRequest setFetchBatchSize:20];
    [fetchRequest setFetchLimit:10];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:[NSManagedObjectContext MR_rootSavingContext] sectionNameKeyPath:nil
                                                   cacheName:@"Product"];
    _fetchedResultsController = theFetchedResultsController;
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
    
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller{
    [self.tableView beginUpdates];
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    [self.tableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type{
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath{
    
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            //[self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [self.tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void) showAlertWithTitle:(NSString *)title message:(NSString*) message cancel:(NSString *)cancel sender:(id) sender{
    if([MTUtils checkIosVersion] <= 7){
        m_alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:sender cancelButtonTitle:cancel otherButtonTitles:nil];
        [m_alertView show];
    }else{
        UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:cancel style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
        }]];
        [sender presentViewController:alertController animated:YES completion:nil];
    }
}

-(void)willMoveToParentViewController:(UIViewController *)parent{
    if(parent != nil){
        [self closeAlertView];
    }
}

- (void)closeAlertView{
    if([MTUtils checkIosVersion] <= 7){
        if(m_alertView != nil){
            [m_alertView dismissWithClickedButtonIndex:-1 animated:NO];
        }
    }else{
        if([[self presentedViewController] respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]){
            [[self presentedViewController] dismissViewControllerAnimated:YES completion:nil];
        }
    }
}
@end
