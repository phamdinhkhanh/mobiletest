//
//  MTAddReviewProduct.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/19/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTAddReviewProduct : UIViewController
@property (nonatomic, assign) BOOL isDetailView;
@property (nonatomic, strong) NSString* productId;
@property (nonatomic, strong) NSString* productName;
@end
