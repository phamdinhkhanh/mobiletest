//
//  ServiceHelper.m
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import "ServiceHelper.h"
#import "MTTranslatorHelper.h"
#import "ProductModel.h"
#import "BrandModel.h"
#import "ReviewModel.h"
#import "UserModel.h"

#define HOST @"https://api.parse.com"
#define GET_ALL_PRODUCT @"/1/classes/Product"
#define GET_ALL_BRAND @"/1/classes/Brand"
#define GET_ALL_REVIEW @"/1/classes/Review"
#define GET_ALL_USER @"/1/classes/User"
#define POST_REVIEW @"/1/classes/Review"

@interface ServiceHelper()
@property (nonatomic, strong) MTTranslatorHelper* translatorHelper;

@end

@implementation ServiceHelper

- (MTTranslatorHelper *)translatorHelper
{
    if (_translatorHelper == nil) {
        _translatorHelper = [[MTTranslatorHelper alloc] init];
    }
    return _translatorHelper;
}

-(void)getAllProductsWithOnCompleteBlock:(MTResultsBlock)block
{
    AFHTTPRequestOperationManager *manager  = [self setUpAFHTTPRequestOperationManager];
    [manager GET:[NSString stringWithFormat:@"%@%@",HOST, GET_ALL_PRODUCT ] parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSDictionary* response = ((NSDictionary*)responseObject)[@"results"];
        NSArray* products = [self.translatorHelper translateCollectionFromJSON:response withClassName:@"ProductModel"];
        block(products, nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        block(nil, error);
    }];
}

-(void)getAllBrandsWithOnCompleteBlock:(MTResultsBlock)block
{
    AFHTTPRequestOperationManager *manager  = [self setUpAFHTTPRequestOperationManager];
    [manager GET:[NSString stringWithFormat:@"%@%@",HOST, GET_ALL_BRAND ] parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSDictionary* response = ((NSDictionary*)responseObject)[@"results"];
        NSArray* brands = [self.translatorHelper translateCollectionFromJSON:response withClassName:@"BrandModel"];
        block(brands, nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        block(nil, error);
    }];

}

-(void)getAllReviewsWithOnCompleteBlock:(MTResultsBlock)block
{
    AFHTTPRequestOperationManager *manager  = [self setUpAFHTTPRequestOperationManager];
    [manager GET:[NSString stringWithFormat:@"%@%@",HOST, GET_ALL_REVIEW ] parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSDictionary* response = ((NSDictionary*)responseObject)[@"results"];
        NSArray* reviews = [self.translatorHelper translateCollectionFromJSON:response withClassName:@"ReviewModel"];
        block(reviews, nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        block(nil, error);
    }];
}

-(void)getAllReviewsWithProductIdArray:(NSString *)productId OnCompleteBlock:(MTResultsBlock)block{
    AFHTTPRequestOperationManager *manager  = [self setUpAFHTTPRequestOperationManager];
    [manager GET:[NSString stringWithFormat:@"%@%@?limit=500",HOST, GET_ALL_REVIEW ] parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSDictionary* response = ((NSDictionary*)responseObject)[@"results"];
        NSArray* reviews = [self.translatorHelper translateCollectionFromJSON:response withClassName:@"ReviewModel"];
        block(reviews, nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        block(nil, error);
    }];
}

-(void)getAllReviewsWithProductId:(NSString *)productId OnCompleteBlock:(MTResultsBlock)block{
    
    AFHTTPRequestOperationManager *manager  = [self setUpAFHTTPRequestOperationManager];
    NSString* stringParam = [NSString stringWithFormat:@"where={\"productID\":{\"$inQuery\":{\"where\":{\"objectId\":\"%@\"},\"className\":\"Product\"}}}",productId];
    NSString* strURL = [NSString stringWithFormat:@"%@%@?%@",HOST, GET_ALL_REVIEW,stringParam];
    NSMutableCharacterSet *chars = NSCharacterSet.URLQueryAllowedCharacterSet.mutableCopy;
    [chars removeCharactersInRange:NSMakeRange('&', 1)]; // %26
    strURL = [strURL stringByAddingPercentEncodingWithAllowedCharacters:chars];
    
    [manager GET:strURL parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSDictionary* response = ((NSDictionary*)responseObject)[@"results"];
        NSArray* reviews = [self.translatorHelper translateCollectionFromJSON:response withClassName:@"ReviewModel"];
        block(reviews, nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        block(nil, error);
    }];
}


-(void)getAllUsersWithOnCompleteBlock:(MTResultsBlock)block{
    AFHTTPRequestOperationManager *manager  = [self setUpAFHTTPRequestOperationManager];
    [manager GET:[NSString stringWithFormat:@"%@%@",HOST, GET_ALL_USER ] parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSDictionary* response = ((NSDictionary*)responseObject)[@"results"];
        NSArray* users = [self.translatorHelper translateCollectionFromJSON:response withClassName:@"UserModel"];
        block(users, nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        block(nil, error);
    }];
}


-(void)getAllProductsWithBrandName:(NSString *)brandName OnCompleteBlock:(MTResultsBlock)block{
    AFHTTPRequestOperationManager *manager  = [self setUpAFHTTPRequestOperationManager];
    
    NSString* stringParam = [NSString stringWithFormat:@"where={\"brandID\":{\"$inQuery\":{\"where\":{\"name\":\"%@\"},\"className\":\"Brand\"}}}&order=dateCreated&limit=10",brandName];
    NSString* strURL = [NSString stringWithFormat:@"%@%@?%@",HOST, GET_ALL_PRODUCT,stringParam];
    NSMutableCharacterSet *chars = NSCharacterSet.URLQueryAllowedCharacterSet.mutableCopy;
    [chars removeCharactersInRange:NSMakeRange('&', 1)]; // %26
    strURL = [strURL stringByAddingPercentEncodingWithAllowedCharacters:chars];
    
    [manager GET:strURL  parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSDictionary* response = ((NSDictionary*)responseObject)[@"results"];
        NSArray* products = [self.translatorHelper translateCollectionFromJSON:response withClassName:@"ProductModel"];
        block(products, nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        block(nil, error);
    }];
}

-(void)postReview:(NSString *)comment Rating:(int)rating productId:(NSString *)productId userId:(NSString *)userId onCompleteBlock:(MTResponseBlock)block{
    AFHTTPRequestOperationManager *manager  = [self setUpAFHTTPRequestOperationManager];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",HOST, POST_REVIEW]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"no-cache" forHTTPHeaderField:@"Cache-Control"];
    [request setValue:@"MlR6vYpYvLRxfibxE5cg0e73jXojL6jWFqXU6F8L" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:@"7BTXVX1qUXKUCnsngL8LxhpEHKQ8KKd798kKpD9W" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    NSString* stringParam = [NSString stringWithFormat:@"{\"comment\": \"%@\", \"rating\": %d, \"productID\": { \"__type\": \"Pointer\", \"className\": \"Product\", \"objectId\": \"%@\" }, \"userID\": { \"__type\": \"Pointer\", \"className\": \"User\", \"objectId\": \"%@\" }}",comment, rating, productId, userId];
    NSData* data = [stringParam dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    [request setTimeoutInterval:60];

    AFHTTPRequestOperation *operation  = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSLog(@"JSON: %@", responseObject);
        block(YES,nil);
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        block(NO,error);
    }];
    [operation start];
}

-(AFHTTPRequestOperationManager*)setUpAFHTTPRequestOperationManager{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"no-cache" forHTTPHeaderField:@"Cache-Control"];
    [manager.requestSerializer setValue:@"MlR6vYpYvLRxfibxE5cg0e73jXojL6jWFqXU6F8L" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [manager.requestSerializer setValue:@"7BTXVX1qUXKUCnsngL8LxhpEHKQ8KKd798kKpD9W" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    //[manager.requestSerializer setTimeoutInterval:30];
    return manager;
}

@end
