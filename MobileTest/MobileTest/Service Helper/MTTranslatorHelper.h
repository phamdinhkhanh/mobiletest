//
//  DNTranslatorHelper.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface MTTranslatorHelper : NSObject
- (id)translateModelFromJSON:(NSDictionary *)JSON
               withclassName:(NSString *)className;
- (id)translateCollectionFromJSON:(NSDictionary *)JSON
                    withClassName:(NSString *)className;
@end
