//
//  ServiceHelper.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTFetcher.h"
#import "MTPoster.h"

@interface ServiceHelper : NSObject<MTFetcher, MTPoster>

@end
