//
//  MTAppDataManager.m
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import "MTAppDataManager.h"
#import "ServiceHelper.h"
#import "DatabaseHelper.h"
#import "ProductModel.h"
#import "ProductManageObject.h"
#import "BrandModel.h"
#import "BrandManageObject.h"
#import "ReviewModel.h"
#import "ReviewManageObject.h"
#import "UserModel.h"
#import "UserManageObject.h"

@interface MTAppDataManager()
@property (nonatomic, strong) ServiceHelper * serviceHelper;
@property (nonatomic, strong) DatabaseHelper * databaseHelper;
@property (nonatomic, strong) NSMutableSet *currentBrandOperations;
@property (nonatomic, strong) NSOperationQueue *brandOperationQueue;
@end

@implementation MTAppDataManager
+ (id) shareInstance{
    static dispatch_once_t once;
    static MTAppDataManager *shareInstance;
    dispatch_once(&once, ^{
        shareInstance = [[self alloc] init];
    });
    return shareInstance;
}

- (ServiceHelper *) serviceHelper{
    if(!_serviceHelper){
        _serviceHelper = [[ServiceHelper alloc] init];
    }
    return _serviceHelper;
}

- (DatabaseHelper *) databaseHelper{
    if(!_databaseHelper){
        _databaseHelper = [[DatabaseHelper alloc] init];
    }
    return _databaseHelper;
}

#pragma mark - Fetcher From Api -------
-(void)getAllProductsWithOnCompleteBlock:(MTResultsBlock)block
{
    [self.serviceHelper getAllProductsWithOnCompleteBlock:^(NSArray *results, NSError *error) {
        if([results count] > 0){
            NSPredicate* predicate = [NSPredicate predicateWithFormat:@"productName != nil"];
            results =[results filteredArrayUsingPredicate:predicate];
            
            NSArray* productsArray = [ProductManageObject MR_findAll];
            if([productsArray count] > 0){
                NSMutableArray* filterResults = [NSMutableArray arrayWithCapacity:[results count]];
                [results enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSString* objectId = (NSString*)((ProductModel*)obj).objectId;
                    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"objectId == %@", objectId];
                    NSArray* filter = [productsArray filteredArrayUsingPredicate:predicate];
                    if([filter count] == 0)
                        [filterResults addObject:obj];
                }];
                
                if([filterResults count] > 0)
                    [self.databaseHelper storeProducts:filterResults];
            }else{
                [self.databaseHelper storeProducts:results];
            }
        }
        block(results,error);
    }];
}

-(void)getAllBrandsWithOnCompleteBlock:(MTResultsBlock)block
{
    [self.serviceHelper getAllBrandsWithOnCompleteBlock:^(NSArray *results, NSError *error) {
        if([results count] > 0){
            NSPredicate* predicate = [NSPredicate predicateWithFormat:@"nameBrand != nil"];
            results = [results filteredArrayUsingPredicate:predicate];
            
            NSArray* brandArray = [BrandManageObject MR_findAll];
            if([brandArray count] > 0){
                NSMutableArray* filterResults = [NSMutableArray arrayWithCapacity:[results count]];
                [results enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSString* objectId = (NSString*)((BrandModel*)obj).objectId;
                    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"objectId == %@", objectId];
                    NSArray* filter = [brandArray filteredArrayUsingPredicate:predicate];
                    if([filter count] == 0)
                        [filterResults addObject:obj];
                }];
                
                if([filterResults count] > 0)
                    [self.databaseHelper storeBrands:filterResults];
            }else{
                [self.databaseHelper storeBrands:results];
            }
        }
        block(results,error);
    }];
}

//-(void)getAllReviewsWithOnCompleteBlock:(MTResultsBlock)block
//{
//    [self.serviceHelper getAllReviewsWithOnCompleteBlock:^(NSArray *results, NSError *error) {
//        if([results count] > 0){
//            NSPredicate* predicate = [NSPredicate predicateWithFormat:@"comment != nil"];
//            results = [results filteredArrayUsingPredicate:predicate];
//            
//            NSArray* reviewsArray = [ReviewManageObject MR_findAll];
//            if([reviewsArray count] > 0){
//                NSMutableArray* filterResults = [NSMutableArray arrayWithCapacity:[results count]];
//                [results enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                    NSString* objectId = (NSString*)((ReviewModel*)obj).objectId;
//                    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"objectId == %@", objectId];
//                    NSArray* filter = [reviewsArray filteredArrayUsingPredicate:predicate];
//                    if([filter count] == 0)
//                        [filterResults addObject:obj];
//                }];
//                
//                if([filterResults count] > 0)
//                    [self.databaseHelper storeReviews:filterResults];
//            }else{
//                
//                [self.databaseHelper storeReviews:results];
//            }
//        }
//        block(results,error);
//    }];
//    
//}

-(void)getAllReviewsWithProductIdArray:(NSArray *)productsArray OnCompleteBlock:(MTResultsBlock)block{
    [self.serviceHelper getAllReviewsWithProductIdArray:nil OnCompleteBlock:^(NSArray *results, NSError *error) {
        __block NSMutableArray* filterReview;
        if([results count] > 0){
            filterReview = [NSMutableArray arrayWithCapacity:results.count];
            [productsArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ProductModel* productObject = (ProductModel*)obj;
                NSPredicate* predicate = [NSPredicate predicateWithFormat:@"comment != nil AND productId.objectId == %@", productObject.objectId];
                NSArray* filter = [results filteredArrayUsingPredicate:predicate];
                [filterReview addObjectsFromArray:filter];
            }];

            NSArray* reviewsArray = [ReviewManageObject MR_findAll];
            if([reviewsArray count] > 0){
                NSMutableArray* filterResults = [NSMutableArray arrayWithCapacity:[filterReview count]];
                [filterReview enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSString* objectId = (NSString*)((ReviewModel*)obj).objectId;
                    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"objectId == %@", objectId];
                    NSArray* filter = [reviewsArray filteredArrayUsingPredicate:predicate];
                    if([filter count] == 0)
                        [filterResults addObject:obj];
                }];
                
                if([filterResults count] > 0)
                    [self.databaseHelper storeReviews:filterResults];
            }else{
                
                [self.databaseHelper storeReviews:filterReview];
            }
        }
        block(filterReview,error);
    }];
}

-(void)getAllReviewsWithProductId:(NSString *)productId OnCompleteBlock:(MTResultsBlock)block{
    [self.serviceHelper getAllReviewsWithProductId:productId OnCompleteBlock:^(NSArray *results, NSError *error) {
        if([results count] > 0){
            NSPredicate* predicate = [NSPredicate predicateWithFormat:@"comment != nil"];
            results = [results filteredArrayUsingPredicate:predicate];
            
            NSArray* reviewsArray = [ReviewManageObject MR_findAll];
            if([reviewsArray count] > 0){
                NSMutableArray* filterResults = [NSMutableArray arrayWithCapacity:[results count]];
                [results enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSString* objectId = (NSString*)((ReviewModel*)obj).objectId;
                    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"objectId == %@", objectId];
                    NSArray* filter = [reviewsArray filteredArrayUsingPredicate:predicate];
                    if([filter count] == 0)
                        [filterResults addObject:obj];
                }];
                
                if([filterResults count] > 0)
                    [self.databaseHelper storeReviews:filterResults];
            }else{
                
                [self.databaseHelper storeReviews:results];
            }
        }
        block(results,error);
    }];
}

-(void)getAllUsersWithOnCompleteBlock:(MTResultsBlock )block
{
    [self.serviceHelper getAllUsersWithOnCompleteBlock:^(NSArray *results, NSError *error) {
        if([results count] > 0){
            NSPredicate* predicate = [NSPredicate predicateWithFormat:@"userName != nil"];
            results = [results filteredArrayUsingPredicate:predicate];
            
            NSArray* usersArray = [UserManageObject MR_findAll];
            if([usersArray count] > 0){
                NSMutableArray* filterResults = [NSMutableArray arrayWithCapacity:[results count]];
                [results enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSString* objectId = (NSString*)((UserModel*)obj).objectId;
                    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"objectId == %@", objectId];
                    NSArray* filter = [usersArray filteredArrayUsingPredicate:predicate];
                    if([filter count] == 0)
                        [filterResults addObject:obj];
                }];
                
                if([filterResults count] > 0)
                    [self.databaseHelper storeUsers:filterResults];
            }else{
                [self.databaseHelper storeUsers:results];
            }
        }
        block(results,error);
    }];
}

-(void)getAllProductsWithBrandName:(NSString *)brandName OnCompleteBlock:(MTResultsBlock)block{
    [self.serviceHelper getAllProductsWithBrandName:brandName OnCompleteBlock:^(NSArray *results, NSError *error) {
        if([results count] > 0){
            NSPredicate* predicate = [NSPredicate predicateWithFormat:@"productName != nil"];
            results = [results filteredArrayUsingPredicate:predicate];
            
            NSArray* productsArray = [ProductManageObject MR_findAll];
            if([productsArray count] > 0){
                NSMutableArray* filterResults = [NSMutableArray arrayWithCapacity:[results count]];
                [results enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSString* objectId = (NSString*)((ProductModel*)obj).objectId;
                    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"objectId == %@", objectId];
                    NSArray* filter = [productsArray filteredArrayUsingPredicate:predicate];
                    if([filter count] == 0)
                        [filterResults addObject:obj];
                }];
                
                if([filterResults count] > 0)
                    [self.databaseHelper storeProducts:filterResults];
            }else{
                [self.databaseHelper storeProducts:results];
            }
        }
        block(results,error);
    }];
}

#pragma mark - Post to API ------
-(void)postReview:(NSString *)comment Rating:(int)rating productId:(NSString *)productId userId:(NSString *)userId onCompleteBlock:(MTResponseBlock)block{
    [self.serviceHelper postReview:comment Rating:rating productId:productId userId:userId onCompleteBlock:^(BOOL success, NSError *error) {
        block(success, error);
    }];
}

#pragma mark - Fetch from database ------
-(void)getBrandWithObjectId:(NSString *)objectId onComplete:(MTObjectBlock)block{
    [self.databaseHelper getBrandWithObjectId:objectId onComplete:^(id obj, NSError *error) {
        block(obj,error);
    }];
}

-(void)getUserWithObjectId:(NSString *)objectId onComplete:(MTObjectBlock)block{
    [self.databaseHelper getUserWithObjectId:objectId onComplete:^(id obj, NSError *error) {
        block(obj,error);
    }];
}

-(void)getBrandWithbrandName:(NSString *)brandName onComplete:(MTObjectBlock)block{
    [self.databaseHelper getBrandWithbrandName:brandName onComplete:^(id obj, NSError *error) {
        block(obj,error);
    }];
}

-(int)getReviewCountWithProductId:(NSString*)productId;{
    return [self.databaseHelper getReviewCountWithProductId:productId];
}

-(id)getProductWithProductId:(NSString *)productId{
    return [self.databaseHelper getProductWithProductId:productId];
}

-(NSArray*)getEmail{
    return [self.databaseHelper getEmail];
}

-(id)checkEmailUserIsExist:(NSString *)email{
    return [self.databaseHelper checkEmailUserIsExist:email];
}

#pragma mark -  Store to database -------
-(void)storeBrands:(NSArray *)brandsArray{
    //implement in databaseHelper;
}

-(void)storeProducts:(NSArray *)productsArray{
    
}

-(void)storeReviews:(NSArray *)reviewsArray{
    
}

-(void)storeUsers:(NSArray *)usersArray{
    
}


@end
