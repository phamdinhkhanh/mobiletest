//
//  MTAppDataManager.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTFetcher.h"
#import "MTPoster.h"
#import "MTFetcherDB.h"
#import "MTStorage.h"

@interface MTAppDataManager : NSObject<MTFetcher, MTPoster, MTFetcherDB, MTStorage>
+ (id) shareInstance;
@end
