//
//  MTUtils.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/17/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTUtils : NSObject
+ (NSDate*) dateFormatter:(NSString*)str;
+ (float)checkIosVersion;
+ (BOOL) isValidEmailFormat:(NSString*) email;
+ (int)calculateAverageRating:(NSArray*)arrayReview;
@end
