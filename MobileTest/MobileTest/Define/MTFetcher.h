//
//  MTFetcher.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

@protocol MTFetcher <NSObject>
-(void)getAllProductsWithOnCompleteBlock:(MTResultsBlock)block;
-(void)getAllBrandsWithOnCompleteBlock:(MTResultsBlock)block;
//-(void)getAllReviewsWithOnCompleteBlock:(MTResultsBlock)block;
-(void)getAllReviewsWithProductIdArray:(NSArray*)productsArray OnCompleteBlock:(MTResultsBlock)block;
-(void)getAllReviewsWithProductId:(NSString*)productId OnCompleteBlock:(MTResultsBlock)block;
-(void)getAllUsersWithOnCompleteBlock:(MTResultsBlock)block;
-(void)getAllProductsWithBrandName:(NSString*)brandName OnCompleteBlock:(MTResultsBlock)block;
@end