//
//  MTPoster.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

@protocol MTPoster <NSObject>
-(void)postReview:(NSString*)comment Rating:(int)rating productId:(NSString*)productId userId:(NSString*)userId onCompleteBlock:(MTResponseBlock)block;
@end