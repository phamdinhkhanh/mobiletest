//
//  MTStorage.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

@protocol MTStorage <NSObject>
-(void)storeProducts:(NSArray*)productsArray;
-(void)storeBrands:(NSArray*)brandsArray;
-(void)storeReviews:(NSArray*)reviewsArray;
-(void)storeUsers:(NSArray*)usersArray;
@end
