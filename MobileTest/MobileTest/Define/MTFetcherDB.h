//
//  MTFetcherDB.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/18/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

@protocol MTFetcherDB <NSObject>
-(void)getBrandWithObjectId:(NSString*)objectId onComplete:(MTObjectBlock)block;
-(void)getBrandWithbrandName:(NSString*)brandName onComplete:(MTObjectBlock)block;
-(void)getUserWithObjectId:(NSString*)objectId onComplete:(MTObjectBlock)block;
-(int)getReviewCountWithProductId:(NSString*)productId;
-(NSArray*)getEmail;
-(id)getProductWithProductId:(NSString*)productId;
-(id)checkEmailUserIsExist:(NSString*)email;
@end