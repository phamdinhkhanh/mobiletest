//
//  MTResponseBlock.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

typedef void (^MTSuccessBlock)();
typedef void (^MTResultJSONBlock)(NSDictionary *result, NSError* error);
typedef void (^MTResultsSuccessBlock)(NSArray *results);
typedef void (^MTResultsBlock)(NSArray *results, NSError* error);
typedef void (^MTFailureBlock)(NSError *error);
typedef void (^MTResponseBlock)(BOOL success, NSError *error);
typedef void (^MTResponseWithMessageBlock)(NSString* message, BOOL success, NSError *error);
typedef void (^MTObjectBlock)(id obj, NSError *error);

