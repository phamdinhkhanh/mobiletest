//
//  MTUtils.m
//  MobileTest
//
//  Created by PDInhKhanh on 2/17/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import "MTUtils.h"
#import "ReviewManageObject.h"
@implementation MTUtils
+ (NSDate*) dateFormatter:(NSString*)str{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ex_US_POSIX"];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    
    return [dateFormatter dateFromString:str];
}

+ (float)checkIosVersion{
    return [[[UIDevice currentDevice] systemVersion] intValue];
}

+ (BOOL) isValidEmailFormat:(NSString*) email{
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
    return [emailTest evaluateWithObject:email];
    
}

+(int)calculateAverageRating:(NSArray*)arrayReview{
    __block int numberRating = 0;
    if(arrayReview.count > 0){
        [arrayReview enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ReviewManageObject* reviewObject = (ReviewManageObject*)obj;
            numberRating += [reviewObject.rating intValue];
        }];
        return numberRating;
    }
    return 0;
}

@end
