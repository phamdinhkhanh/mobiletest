//
//  BrandManageObject+CoreDataProperties.m
//  MobileTest
//
//  Created by PDInhKhanh on 2/20/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BrandManageObject+CoreDataProperties.h"

@implementation BrandManageObject (CoreDataProperties)

@dynamic descBrand;
@dynamic name;
@dynamic objectId;
@dynamic product;

@end
