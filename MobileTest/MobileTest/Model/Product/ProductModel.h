//
//  ProductModel.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "BrandIDModel.h"
#import "DateCreateModel.h"

@interface ProductModel : MTLModel<MTLJSONSerializing>
@property (nullable, nonatomic, strong) NSString* availabilityStatus;
@property (nullable, nonatomic, strong) NSString* colour;
@property (nullable, nonatomic, strong) DateCreateModel* dateCreated;
@property (nullable, nonatomic, strong) NSString* descProduct;
@property (nullable, nonatomic, strong) NSString* objectId;
@property (nullable, nonatomic, strong) NSString* productName;
@property (nullable, nonatomic, strong) BrandIDModel* brandId;
@property (nullable, nonatomic, assign) NSNumber* price;
@end
