//
//  ProductModel.m
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import "ProductModel.h"

@implementation ProductModel

+(NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{
             @"availabilityStatus"  : @"availabilityStatus",
             @"brandId"             : @"brandID",
             @"colour"              : @"colour",
             @"dateCreated"         : @"dateCreated",
             @"descProduct"         : @"description",
             @"objectId"            : @"objectId",
             @"productName"         : @"productName",
             @"price"               : @"price",
             };
}

+ (NSValueTransformer *)brandIdJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:BrandIDModel.class];
}

+ (NSValueTransformer *)dateCreatedJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:DateCreateModel.class];
}

@end
