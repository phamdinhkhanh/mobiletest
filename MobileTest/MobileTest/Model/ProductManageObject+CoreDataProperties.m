//
//  ProductManageObject+CoreDataProperties.m
//  MobileTest
//
//  Created by PDInhKhanh on 2/20/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ProductManageObject+CoreDataProperties.h"

@implementation ProductManageObject (CoreDataProperties)

@dynamic availabilityStatus;
@dynamic colour;
@dynamic dateCreated;
@dynamic descProduct;
@dynamic objectId;
@dynamic price;
@dynamic productName;
@dynamic brand;

@end
