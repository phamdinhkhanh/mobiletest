//
//  BrandManageObject+CoreDataProperties.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/20/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BrandManageObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface BrandManageObject (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *descBrand;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *objectId;
@property (nullable, nonatomic, retain) NSSet<ProductManageObject *> *product;

@end

@interface BrandManageObject (CoreDataGeneratedAccessors)

- (void)addProductObject:(ProductManageObject *)value;
- (void)removeProductObject:(ProductManageObject *)value;
- (void)addProduct:(NSSet<ProductManageObject *> *)values;
- (void)removeProduct:(NSSet<ProductManageObject *> *)values;

@end

NS_ASSUME_NONNULL_END
