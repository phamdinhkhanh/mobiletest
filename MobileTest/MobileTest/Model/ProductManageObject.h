//
//  ProductManageObject.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/20/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BrandManageObject;

NS_ASSUME_NONNULL_BEGIN

@interface ProductManageObject : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "ProductManageObject+CoreDataProperties.h"
