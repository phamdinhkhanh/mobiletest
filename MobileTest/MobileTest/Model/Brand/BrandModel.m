//
//  BrandModel.m
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import "BrandModel.h"

@implementation BrandModel

+(NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{
             @"objectId"  : @"objectId",
             @"nameBrand" : @"name",
             @"descBrand" : @"description"
             };
}

@end
