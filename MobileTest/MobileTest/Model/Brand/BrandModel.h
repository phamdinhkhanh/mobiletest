//
//  BrandModel.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface BrandModel : MTLModel<MTLJSONSerializing>
@property (nullable, nonatomic, strong) NSString* objectId;
@property (nullable, nonatomic, strong) NSString* nameBrand;
@property (nullable, nonatomic, strong) NSString* descBrand;
@end
