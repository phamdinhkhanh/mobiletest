//
//  ReviewManageObject+CoreDataProperties.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/20/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ReviewManageObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReviewManageObject (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *comment;
@property (nullable, nonatomic, retain) NSString *objectId;
@property (nullable, nonatomic, retain) NSNumber *rating;
@property (nullable, nonatomic, retain) NSDate *updateAt;
@property (nullable, nonatomic, retain) ProductManageObject *product;
@property (nullable, nonatomic, retain) UserManageObject *user;

@end

NS_ASSUME_NONNULL_END
