//
//  UserIDModel.m
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import "UserIDModel.h"

@implementation UserIDModel
+(NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{
             @"type"        : @"__type",
             @"className"   : @"className",
             @"objectId"    : @"objectId"
             };
}
@end
