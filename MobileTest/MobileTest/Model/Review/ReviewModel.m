//
//  ReviewModel.m
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import "ReviewModel.h"
#import "MTUtils.h"

@implementation ReviewModel

+(NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{
             @"objectId"    : @"objectId",
             @"rating"      : @"rating",
             @"comment"     : @"comment",
             @"updatedAt"    : @"updatedAt",
             @"productId"   : @"productID",
             @"userId"      : @"userID"
             };
}

+ (NSValueTransformer *)productIdJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:ProductIDModel.class];
}

+ (NSValueTransformer *)userIdJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:UserIDModel.class];
}

@end
