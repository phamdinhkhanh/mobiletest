//
//  ReviewModel.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "ProductIDModel.h"
#import "UserIDModel.h"

@interface ReviewModel : MTLModel<MTLJSONSerializing>
@property (nullable, nonatomic, strong) NSString* objectId;
@property (nullable, nonatomic, assign) NSNumber* rating;
@property (nullable, nonatomic, strong) NSString* comment;
@property (nullable, nonatomic, strong) NSString* updatedAt;
@property (nullable, nonatomic, strong) ProductIDModel* productId;
@property (nullable, nonatomic, strong) UserIDModel* userId;
@end
