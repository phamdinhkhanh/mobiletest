//
//  ProductManageObject+CoreDataProperties.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/20/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ProductManageObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductManageObject (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *availabilityStatus;
@property (nullable, nonatomic, retain) NSString *colour;
@property (nullable, nonatomic, retain) NSDate *dateCreated;
@property (nullable, nonatomic, retain) NSString *descProduct;
@property (nullable, nonatomic, retain) NSString *objectId;
@property (nullable, nonatomic, retain) NSNumber *price;
@property (nullable, nonatomic, retain) NSString *productName;
@property (nullable, nonatomic, retain) BrandManageObject *brand;

@end

NS_ASSUME_NONNULL_END
