//
//  UserModel.m
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import "UserModel.h"
@implementation UserModel

+(NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{
             @"objectId"    : @"objectId",
             @"userName"    : @"userName",
             @"email"       : @"email",
             @"dateOfBirth" : @"dateOfBirth",
             @"isCustomer"  : @"isCustomer"
             };
}

+ (NSValueTransformer *)dateOfBirthJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:DateOfBirth.class];
}

+ (NSValueTransformer *)isCustomerJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLBooleanValueTransformerName];
}

@end
