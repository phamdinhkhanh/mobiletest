//
//  DateOfBirth.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import "MTLModel.h"

@interface DateOfBirth : MTLModel <MTLJSONSerializing>
@property (nonatomic, strong) NSString* type;
@property (nonatomic, strong) NSString* isoDate;
@end
