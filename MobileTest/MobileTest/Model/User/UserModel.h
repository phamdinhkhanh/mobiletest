//
//  UserModel.h
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "DateOfBirth.h"

@interface UserModel : MTLModel<MTLJSONSerializing>
@property (nullable, nonatomic, strong) NSString* objectId;
@property (nullable, nonatomic, assign) NSString* userName;
@property (nullable, nonatomic, strong) NSString* email;
@property (nullable, nonatomic, strong) DateOfBirth* dateOfBirth;
@property (nonatomic, assign) BOOL isCustomer;
@end
