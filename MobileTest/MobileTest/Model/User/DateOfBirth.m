//
//  DateOfBirth.m
//  MobileTest
//
//  Created by PDInhKhanh on 2/16/16.
//  Copyright © 2016 PDInhKhanh. All rights reserved.
//

#import "DateOfBirth.h"

@implementation DateOfBirth
+(NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{
             @"type"      : @"__type",
             @"isoDate"   : @"iso"
             };
}
@end
